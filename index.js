
function defaultConsumer( level, msg, ctx ) {
    console.log( JSON.stringify( { level, msg, ctx } ) );
}


class Logger {

    constructor( { ctx = {}, consumer = defaultConsumer } ) {
        this.ctx      = ctx;
        this.consumer = consumer;
    }

    _message( level, msg, ctx ) {

        this.consumer( level, msg, {
            ts : new Date().getTime(),
            ... this.ctx,
            ... ctx,
        } );

    }

    debug( msg, ctx = {} ) {

        this._message( "DEBUG", msg, ctx );

    }

    info( msg, ctx = {} ) {

        this._message( "INFO", msg, ctx );
    }

    warn( msg, ctx = {} ) {

        this._message( "WARN", msg, ctx );

    }

    error( msg, ctx = {} ) {

        this._message( "ERROR", msg, ctx );

    }

    alert( msg, ctx = {} ) {

        this._message( "ALERT", msg, ctx );
        
    }


}

module.exports = { 
    logger  : new Logger( { } ),
    Logger  : Logger
};
